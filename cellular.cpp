#include <iostream>
#include <fstream>
#include <stdexcept>
#include "portable_bitmap.h"


using namespace std;


bool transition(bool left, bool center, bool right);


int main()
{
    char const characterMap[] = { '_', '#' };
    int const maxGenerations = 150;
    int const cellCount = 100;

    bool universe[cellCount] =
        { 0, 1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0,
          0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1 };
    bool tempUniverse[cellCount] = { 0 };

    // File opening
    ofstream f;
    f.open("dev_null");
    if (!f.is_open()) {
        cout << "[!] Doom has risen." << endl;
        exit(1);
    }

    // Portable Bitmap
    PortableBitmap pb(maxGenerations, cellCount);
    for (int i = 0; i < maxGenerations; ++i) {
        // output
        for (int j = 0; j < cellCount; ++j) {
            cout << characterMap[universe[j]];
            f << characterMap[universe[j]];

            // PB insert
            pb.setPixel(universe[j], j, i);
        }
        cout << endl;
        f << endl;

        // update
        tempUniverse[0] =
            transition(universe[cellCount - 1], universe[0], universe[1]);
        for (int j = 1; j < cellCount - 1; ++j) {
            tempUniverse[j] =
                transition(universe[j - 1], universe[j], universe[j + 1]);
        }
        tempUniverse[cellCount - 1] =
            transition(universe[cellCount - 2], universe[cellCount - 1], universe[0]);
        // copy
        for (int j = 0; j < cellCount; ++j) {
            universe[j] = tempUniverse[j];
        }
    }

    // Save PB
    try {
        pb.save("doom.pbm");
    } catch (invalid_argument& ex) {
        cout << ex.what() << endl;
    } catch (runtime_error& ex) {
        cout << ex.what() << endl;
    }

    // Load PB
    try {
        PortableBitmap pbl = pb.load("doom.pbm");
        pbl.save("doomx.pbm");
    } catch (invalid_argument& ex) {
        cout << ex.what() << endl;
    } catch (runtime_error& ex) {
        cout << ex.what() << endl;
    }

    // Close file
    f.close();

    return 0;
}


bool transition(bool left, bool center, bool right)
{
    static bool const transitionMap[] = { 0, 1, 1, 1, 0, 1, 1, 0 };

    int inputEncoded = right + 2 * center + 4 * left;
    return transitionMap[inputEncoded];
}

