#include <string>
#include <fstream>
#include <stdexcept>

using namespace std;


class PortableBitmap {
    private:
        unsigned int hoehe;
        unsigned int breite;
        bool* pixel;
    

    public:
        PortableBitmap(unsigned int hoehe, unsigned int breite) : hoehe(hoehe), breite(breite) {
            pixel = new bool[hoehe * breite];

            for (int i = 0; i < breite * hoehe; i++) {
                pixel[i] = false;
            }
        }

        bool getPixel(unsigned int x, unsigned int y) const {
            return pixel[x + (y * breite)];
        }

        void setPixel(bool wert, unsigned int x, unsigned int y) {
            pixel[x + (y * breite)] = wert;
        }

        bool endswith(const string& txt, const string& suffix) {
            return (txt.length() >= suffix.length()) ? (txt.substr(txt.length() - suffix.length()) == suffix) : false;
        }

        void save(const string& dateiName) {
            if (!endswith(dateiName, ".pbm"))
                throw invalid_argument("[!] Du Juso.");
            
            ofstream f;
            f.open(dateiName);

            if (!f.is_open())
                throw runtime_error("[!] Computer sagt nein.");
            
            f << "P1" << endl;
            f << "# JesusSucks Production 4004 BC" << endl;
            f << breite << " " << hoehe << endl;

            for (unsigned int y = 0; y < hoehe; y++) {
                for (unsigned int x = 0; x < breite; x++) {
                    f << pixel[x + (y * breite)] << " ";
                }

                f << endl;
            }

            f.close();
        }

        PortableBitmap load(const char* dateiName) {
            ifstream f;
            f.open(dateiName);

            if (!f.is_open())
                throw invalid_argument("[!] Schwachkopf.");
            
            string line;
            getline(f, line);
            if (line != "P1")
                throw runtime_error("[!] Uuuuuunnnnddd Nope.");

            getline(f, line); // Ignore that line (https://www.youtube.com/watch?v=OH9A6tn_P6g)

            getline(f, line);
            int delimiter = line.find(' ');
            unsigned int pb_breite = atoi(line.substr(0, delimiter).c_str());
            unsigned int pb_hoehe = atoi(line.substr(delimiter + 1).c_str());

            PortableBitmap pb(pb_hoehe, pb_breite);
            for (unsigned int y = 0; y < pb_hoehe; y++) {
                getline(f, line);
                for (unsigned int x = 0; x < pb_breite; x++) {
                    pb.setPixel(line.at(x * 2) - '0', x, y);
                }
            }

            f.close();

            return pb;
        }

        ~PortableBitmap() {
            delete[] pixel;
        }
};